# Mindera Nginx Cookbook

Mindera wrapper cookbook to install [Nginx](http://nginx.org/) using the community cookbook [miketheman/nginx](https://github.com/miketheman/nginx).

## Requirements

Depends on the cookbooks:

 * nginx

## Platforms

 * Centos 6+
 * Amazon Linux - (WIP)

## Attributes

- `node['nginx']['repo_source']` - Defines which yum repo to use, defaults to `nginx`
- `node['nginx']['install_method']` - Defines the install method, defaults to `package`
- `node['nginx']['default_site_enabled']` - Enables the default nginx site, defaults to `false`

## Recipes

### default

Installs nginx from the upstream nginx yum repo with a minimal configuration.

## Providers

Providers are available to perform common tasks related with the nginx installation.

### mind\_nginx\_site

This resource will configure a new nginx site from a minimal configuration template or from a template on the calling cookbook.

Actions:

- `create` - Create a site - see attributes below
- `destroy` - Destroy a site - removes all configurations, data and logs
- `enable` - Enable a site
- `disable` - Disable a site

Attributes:

- `name` - Name of the nginx site - this will be used to create config files, site and log directories - required
- `enable` - Reloads nginx to enable the site, defaults to `false`
- `log_dir` - Create the site log directory - it will follow the format `/var/log/nginx/site_name`, defaults to `true`
- `root_dir` - Sets the nginx root where the site data directory will be created and served - it will follow the format `root_dir/site_name`, defaults to `/var/www/nginx`
- `listen_port` - The port in which the site will be listening, defaults to a random port between `8080-9090`
- `from_template` - Uses a custom template from the calling cookbook, defaults to `false` - using this attribute will override all the other except `enable`

Usage:

```ruby
mind_nginx_site 'mysite' do
  listen_port 8080
  log_dir true
  root_dir '/var/www/nginx'
  enable true
end
```

This will create a new `mysite` site listening on port `8080` and enable it.

```ruby
mind_nginx_site 'mysitefromtemplate' do
  from_template true
  enable true
end
```

This will create a new `mysitefromtemplate` site from a custom template and enable it.

This assumes you have a template file in your cookbook following the structure `templates/default/nginx/site\_name.conf.erb`.

## Development / Contributing

### Tests

This assumes you're using [chef-dk](https://downloads.getchef.com/chef-dk/).

To run unit tests using chefspec you can run:
```bash
$ chef exec rspec
```

To run integration tests using kitchen-ci and serverspec you can run:
```bash
$ chef exec kitchen test
```


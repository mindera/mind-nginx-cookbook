require 'serverspec'

# Required by serverspec
set :backend, :exec

RSpec.configure do |c|
  c.before :all do
    c.path = '/sbin:/usr/sbin:$PATH'
  end
end

describe 'nginx' do
  it 'should be enabled' do
    expect(service('nginx')).to be_enabled
  end

  it 'should be running' do
    expect(service('nginx')).to be_running
  end

  it 'should not have the default nginx server config' do
    expect(file('/etc/nginx/conf.d/default.conf')).not_to be_file
  end
end

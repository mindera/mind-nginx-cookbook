if defined?(ChefSpec)
  def create_mind_nginx_site(name)
    ChefSpec::Matchers::ResourceMatcher.new(:mind_nginx_site, :create, name)
  end

  def destroy_mind_nginx_site(name)
    ChefSpec::Matchers::ResourceMatcher.new(:mind_nginx_site, :destroy, name)
  end

  def enable_mind_nginx_site(name)
    ChefSpec::Matchers::ResourceMatcher.new(:mind_nginx_site, :enable, name)
  end

  def disable_mind_nginx_site(name)
    ChefSpec::Matchers::ResourceMatcher.new(:mind_nginx_site, :disable, name)
  end
end

#
# Cookbook Name:: mind-nginx
# Recipe:: default
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Install nginx
include_recipe 'nginx'

# Define nginx service
service 'nginx' do
  supports restart: true, start: true, stop: true, status: true, reload: true
  action :enable
end

# Remove default nginx server config
file "#{node['nginx']['dir']}/conf.d/default.conf" do
  action :delete
  backup 1
  notifies :reload, 'service[nginx]', :delayed
end


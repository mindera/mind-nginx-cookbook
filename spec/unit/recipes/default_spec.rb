require 'spec_helper'

describe 'mind-nginx::default' do
  cached(:chef_run) { ChefSpec::Runner.new.converge(described_recipe) }
  cached(:node) { chef_run.node }
  cached(:default_server) { chef_run.file('/etc/nginx/conf.d/default.conf') }

  it 'include the nginx recipe' do
    expect(chef_run).to include_recipe('nginx')
  end

  it 'define and enable the nginx service' do
    expect(chef_run).to enable_service('nginx')
  end

  it 'delete the nginx default server config' do
    expect(chef_run).to delete_file('/etc/nginx/conf.d/default.conf').with(backup: 1)
    expect(default_server).to notify('service[nginx]').to(:reload).delayed
  end
end

#
# Cookbook Name:: mind-nginx
# Provider:: site
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

def whyrun_supported?
  true
end

use_inline_resources

def nginx_service
  # Define nginx service
  service 'nginx' do
    supports restart: true, start: true, stop: true, status: true, reload: true
    action :nothing
  end
end

def get_random_listen_port
  Random.rand(8080..9090)
end

action :create do
    resource = @new_resource
    Chef::Log.info "Creating nginx site #{resource.name}"
    nginx_service

    if resource.from_template == true
      # Deploy custom nginx site configuration
      template "/etc/nginx/sites-available/#{resource.name}" do
        source "nginx/#{resource.name}.conf.erb"
        cookbook self.cookbook_name.to_s
        mode '0644'
        backup false
        notifies :reload, 'service[nginx]', :delayed
      end
    else
      # Define random listen port if not defined
      resource.listen_port = get_random_listen_port unless resource.listen_port

      # Deploy default nginx site configuration
      template "/etc/nginx/sites-available/#{resource.name}" do
        source "site.conf.erb"
        variables({
          :config => resource
        })
        cookbook 'mind-nginx'
        mode '0644'
        backup false
        notifies :reload, 'service[nginx]', :delayed
      end
    end

    # Create the nginx site dir
    directory "/var/www/nginx/#{resource.name}" do
      action :create
      recursive true
    end

    # Create the nginx site log directory
    if resource.log_dir == true
      directory "/var/log/nginx/#{resource.name}" do
        action :create
        recursive true
      end
    end

    # Enable the nginx site
    if resource.enable == true
      nginx_site resource.name do
        action :enable
        timing :delayed
      end
    end
end

action :destroy do
    resource = @new_resource
    Chef::Log.info "Deleting nginx site #{resource.name}"
    nginx_service

    # Disable the nginx site
    nginx_site resource.name do
      action :disable
    end

    # Remove nginx site config files
    file "/etc/nginx/sites-available/#{resource.name}" do
      action :delete
      notifies :reload, 'service[nginx]', :delayed
    end

    # Remove nginx site log directory
    %w{
      /var/log/nginx
      /var/www/nginx
    }.each do |dir|
      directory "#{dir}/#{resource.name}" do
        action :delete
        recursive true
      end
    end
end

action :enable do
    resource = @new_resource
    Chef::Log.info "Enabling nginx site #{resource.name}"
    nginx_service

    # Enable the nginx site
    nginx_site resource.name do
      action :enable
    end
end

action :disable do
    resource = @new_resource
    Chef::Log.info "Disabling nginx site #{resource.name}"
    nginx_service

    # Disable the nginx site
    nginx_site resource.name do
      action :disable
    end
end


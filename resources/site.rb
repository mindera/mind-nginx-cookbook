#
# Cookbook Name:: mind-nginx
# Resource:: site
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
actions :create, :destroy, :enable, :disable
default_action :create

attribute :name, :name_attribute => true, :kind_of => String
attribute :enable, :kind_of => [TrueClass, FalseClass], :default => false
attribute :log_dir, :kind_of => [TrueClass, FalseClass], :default => true
attribute :root_dir, :kind_of => String, :default => '/var/www/nginx'
attribute :listen_port, :kind_of => Integer
attribute :extra_conf, :kind_of => String
attribute :from_template, :kind_of => [TrueClass, FalseClass], :default => false


name             'mind-nginx'
maintainer       'Mindera'
maintainer_email 'miguel.fonseca@mindera.com'
license          'Apache 2.0'
description      'Installs/Configures Nginx'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'nginx', '>= 2.7.6'

supports 'centos', '~> 6.0'
supports 'amazon'
